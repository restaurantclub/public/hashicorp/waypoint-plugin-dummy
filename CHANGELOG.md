# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.5](https://gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/compare/v0.1.4...v0.1.5) (2021-04-29)


### Bug Fixes

* deploy id ([b69c54a](https://gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/commit/b69c54a2bdc18418e126c303ec8f11afd74f5d15))

### [0.1.4](https://gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/compare/v0.1.3...v0.1.4) (2021-04-29)

### [0.1.3](https://gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/compare/v0.1.2...v0.1.3) (2021-03-08)


### Bug Fixes

* package name typo fix ([3220e95](https://gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/commit/3220e9528bc55e9c963e92ade99eedd337136cf0))

### [0.1.1](https://gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/compare/v0.1.0...v0.1.1) (2021-03-05)

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
