// Package files contains a component for validating local files.
package main

import (
	sdk "github.com/hashicorp/waypoint-plugin-sdk"
	"gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/builder"
	"gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/platform"
	"gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/release"
)

func main() {
	sdk.Main(sdk.WithComponents(
		// Comment out any components which are not
		// required for your plugin
		&builder.Builder{},
		&platform.Platform{},
		&release.Manager{},
	))
}
