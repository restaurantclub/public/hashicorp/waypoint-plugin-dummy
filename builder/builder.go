package builder

import (
	"context"

	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
)

// Builder type
type Builder struct {
	config BuildConfig
}

// BuildConfig type
type BuildConfig struct {
}

// Config implement
func (b *Builder) Config() (interface{}, error) {
	return &b.config, nil
}

// ConfigSet implement
func (b *Builder) ConfigSet(config interface{}) error {
	// config validated ok
	return nil
}

// BuildFunc implement
func (b *Builder) BuildFunc() interface{} {
	// return a function which will be called by Waypoint
	return b.build
}

// Build func
func (b *Builder) build(ctx context.Context, ui terminal.UI) (*Binary, error) {
	u := ui.Status()
	defer u.Close()
	u.Update("Building application")
	u.Step(terminal.StatusOK, "Application built successfully")

	return &Binary{}, nil
}
