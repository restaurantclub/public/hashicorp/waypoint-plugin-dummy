package release

import (
	"context"

	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
)

// Implement the Destroyer interface
func (rm *Manager) DestroyFunc() interface{} {
	return rm.destroy
}

func (rm *Manager) destroy(ctx context.Context, ui terminal.UI, release *Release) error {
	return nil
}
