package release

import (
	"context"
	"fmt"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/component"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy/platform"
)

// Config implement
type Config struct {
}

// Manager init
type Manager struct {
	config Config
}

// Config implement
func (rm *Manager) Config() interface{} {
	return &rm.config
}

// ConfigSet implement
func (rm *Manager) ConfigSet(config interface{}) error {
	_, ok := config.(*Config)
	if !ok {
		// The Waypoint SDK should ensure this never gets hit
		return fmt.Errorf("Expected *ReleaseConfig as parameter")
	}

	// validate the config

	return nil
}

// ReleaseFunc implement
func (rm *Manager) ReleaseFunc() interface{} {
	// return a function which will be called by Waypoint
	return rm.release
}

func (r *Release) URL() string { return r.Url }
func (r *Release) ID() string  { return r.Id }

func (rm *Manager) release(
	ctx context.Context,
	log hclog.Logger,
	src *component.Source,
	ui terminal.UI,
	deploy *platform.Deployment,
) (*Release, error) {
	log.Info("No load-balancer configured")
	return &Release{}, nil
}

var (
	_ component.ReleaseManager = (*Manager)(nil)
)
