package platform

import (
	"context"
	"time"

	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/waypoint-plugin-sdk/component"
	"github.com/hashicorp/waypoint-plugin-sdk/terminal"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Platform implement
type Platform struct {
	config DeployConfig
}

// DeployConfig implement
type DeployConfig struct {
}

// Config implement
func (p *Platform) Config() (interface{}, error) {
	return &p.config, nil
}

// DeployFunc implement
func (p *Platform) DeployFunc() interface{} {
	return p.deploy
}

// deploy function
func (p *Platform) deploy(
	ctx context.Context,
	log hclog.Logger,
	src *component.Source,
	job *component.JobInfo,
	deployConfig *component.DeploymentConfig,
	ui terminal.UI,
) (*Deployment, error) {
	sg := ui.StepGroup()
	defer sg.Wait()

	step := sg.Add("Generating deploy ID")

	defer func() {
		step.Abort()
	}()

	deployment := &Deployment{}
	id, err := component.Id()

	name := id

	s := sg.Add("App deployed as : " + name)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "unable to start container: %s", err)
	}
	s.Done()

	time.Sleep(1 * time.Second)

	step.Done()
	deployment.Id = deployConfig.Env()["WAYPOINT_DEPLOYMENT_ID"]
	deployment.Token = deployConfig.Env()["WAYPOINT_CEB_INVITE_TOKEN"]
	return deployment, nil
}
