module gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy

go 1.14

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/hashicorp/go-hclog v0.16.0
	github.com/hashicorp/waypoint-plugin-sdk v0.0.0-20210422182623-4275ad99673a
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
)

replace gitlab.com/restaurantclub/public/hashicorp/waypoint-plugin-dummy => ./
